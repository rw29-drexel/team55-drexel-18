﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_controls : MonoBehaviour
{

    Animator anim;
    public int playerSpeed = 10; //how fast the player accelerates

    public float playerJumpPower; //how high the player jumps
    bool grounded = false;

    public bool facingRight = true; //<<<<<<< HEAD
    private float moveX; //holds player X input
    public Rigidbody2D rb; //player physics
    public moveHPbar mp;
    public bool movementImpaired = false;
	public wallCheck feet;

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        grounded = feet.wallTouch;
        PlayerMove();
        //Animations
        anim.SetFloat("speed", Mathf.Abs(moveX));


        if (Input.GetKeyDown(KeyCode.Space))
        {
            //anim.SetBool("jumpBool", true);
        }
        else if (grounded)
        {
            //anim.SetBool("jumpBool", false);
        }


        if (Input.GetKeyDown(KeyCode.S))
        {
            movementImpaired = true;
            anim.SetBool("crouchBool", true);
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            movementImpaired = false;
            anim.SetBool("crouchBool", false);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            rb.AddForce(new Vector2(0, playerJumpPower * rb.mass), ForceMode2D.Impulse);
        }
    }

    private void PlayerMove()
    { //movement in X axis
        //Controls, Animations, Player Direction, Physics
        //Controls
        if (movementImpaired == false)
        {
            moveX = Input.GetAxis("Horizontal"); //gets how much and which way the player wants to move in 
        }
        
        //Player Direction
        if (moveX < 0.0f && facingRight == false)
        {
            FlipPlayer();
        }
        else if (moveX > 0.0f && facingRight == true)
        {
            FlipPlayer();
        }

        //Physics
        if (movementImpaired == false)
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
        }
    }

    private void FlipPlayer()
    { //changes direction
        facingRight = !facingRight;
        Vector2 localScale = gameObject.transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }


}
