﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerCaught : MonoBehaviour {

	public static bool PlayerSeen;
	public static uint timer;
	public uint justLetMeSeeStatics;
	// Use this for initialization
	void Start () {
		Reset ();
	}

	public void Reset(){ //resets state. used for new stages or something
		PlayerSeen = false;
		timer = 0;
	}

	public void PlayerFound(uint count){ //use if anything finds the player
		PlayerSeen = true;
		timer = count;
	}

	// Update is called once per frame
	void Update () {
		justLetMeSeeStatics=timer;
		if (PlayerSeen == true) {
			if (timer > 0) { //count down
				timer--;
			} else { //end timer
				PlayerSeen = false;
			}
		}
	}
}
