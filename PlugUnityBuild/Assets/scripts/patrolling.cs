﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class patrolling : MonoBehaviour {

	public wallCheck rightHand;
	public wallCheck leftHand;
	public bool movingRight;
	public Rigidbody2D rb;
	public Transform sightLine;
	
	// Update is called once per frame
	void FixedUpdate () {
		if (movingRight == true && rightHand.wallTouch == true) {
			movingRight = false;
			rb.velocity = new Vector2(0, rb.velocity.y);
		} else if (movingRight == false && leftHand.wallTouch == true) {
			movingRight = true;
			rb.velocity = new Vector2(0, rb.velocity.y);
		}
		if (movingRight == true) {
			rb.AddForce (new Vector2(50,0));
			if (sightLine.rotation.eulerAngles.z != 90) {
				sightLine.Rotate(new Vector3(sightLine.rotation.x, sightLine.rotation.y, 180));
				sightLine.position= sightLine.position + new Vector3(4.5f, 0 ,0);
			}
		} else {
			rb.AddForce (new Vector2(-50,0));
			if (sightLine.rotation.eulerAngles.z != 270) {
				sightLine.Rotate(new Vector3(sightLine.rotation.x, sightLine.rotation.y, -180));
				sightLine.position= sightLine.position + new Vector3(-4.5f, 0 ,0);
			}
		}
	}
}
