﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
static class StupidUnity{
	
	public static void Shuffle<T>(this IList<T> ts) {
		var count = ts.Count;
		var last = count - 1;
		for (var i = 0; i < last; ++i) {
			var r = UnityEngine.Random.Range(i, count);
			var tmp = ts[i];
			ts[i] = ts[r];
			ts[r] = tmp;
		}
	}
}

public class makeMap : MonoBehaviour {

	public Transform me;

	void Start () { //make sure it is always the starting scene with this script
		// PLEASE NOTE
		// ALL scenes have to be 19.5 units long
		// the player has to be able to get to the next part so each scenes should
		//		start and end at 4.5 units on the y axis
		//		a variation of half the normal jump height up and down allowed aswell as a variation of half the defualt jump length is allowed
		//		maps with both variation in height and distance should be tested with all other maps and tagged for later tests by similar maps

		int sceneCount = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings-1; //to ignore itself
		string[] scenes = new string[sceneCount];
		for( int i = 1; i <= sceneCount; i++ ) //get all scenes but itself
		{
			scenes[i-1] = System.IO.Path.GetFileNameWithoutExtension( UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex( i ) );
		}
		StupidUnity.Shuffle (scenes);
		GameObject[] temp;
		float adjustmentX = (me.Find("end").transform.position.x);
		float adjustmentY = (me.Find("end").transform.position.y);
		for (int i = 0; i < scenes.GetLength(0); i++) {
			temp=UnityEngine.SceneManagement.SceneManager.GetSceneByName(scenes [i]).GetRootGameObjects();
			for (int j = 0; j < temp.GetLength (0); j++) {
				if (temp [j].name == "GameObject") {
					adjustmentX += (temp [j].transform.position.x - temp [j].transform.Find("begining").transform.position.x);
					adjustmentY += (temp [j].transform.position.y - temp [j].transform.Find("begining").transform.position.y);
				}
			}
			for (int j = 0; j < temp.GetLength (0); j++) {
				temp [j].transform.position = new Vector3 ((float)(temp [j].transform.position.x + adjustmentX), (float)(temp [j].transform.position.y + adjustmentY), temp [j].transform.position.z);
			}
			for (int j = 0; j < temp.GetLength (0); j++) {
				if (temp [j].name == "GameObject") {
					adjustmentX += temp [j].transform.Find("end").transform.position.x - temp [j].transform.position.x ;
					adjustmentY += temp [j].transform.Find("end").transform.position.y - temp [j].transform.position.y ;
				}
			}
		}
	}
}
