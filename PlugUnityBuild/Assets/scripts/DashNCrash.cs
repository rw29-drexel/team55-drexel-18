﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DashNCrash : MonoBehaviour {
    Animator anim;
    public GameObject Player; //refers tp the Player
    System.Timers.Timer tD; //Timer to invoke new event every interval
    private int chargeDuration; //The Charge Duration
    public bool isHeld; //Checks if the key is Held/Pressed down and held
    public Vector2 acceleration; //Acceleration added as a force
    private float tempX; //Getting new direction movement input

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        Player_controls control = Player.GetComponent<Player_controls>(); //refers to the Player_Controls Script
        control.movementImpaired = false; //movement is allowed
        isHeld = false; //checks if key is pressed
        chargeDuration = 0;
	}
	
    void StartTimer()
    {
        tD = new System.Timers.Timer(); //Timer 
        tD.Interval = 1000f; //1 Second
        tD.Elapsed += new System.Timers.ElapsedEventHandler(ChargeDuration); //Every interval, chargeDuration is invoked
        tD.AutoReset = false; //Resets the interval
        tD.Enabled = true; //Start the timer
    }
    void Charge()
    {
        Player_controls control = Player.GetComponent<Player_controls>(); //refers to the Player_Controls Script 
        if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl)) //If Control Key is pressed down
        {
            anim.SetBool("BoxBox", true);
            Start(); //Initialize
        }

        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) //If Control Key is held
        {
            StartTimer();
            control.movementImpaired = true; //Movement is forbidden while charging
            isHeld = true; //Key is Held Down
        }
        if (Input.GetKeyUp(KeyCode.LeftControl) || Input.GetKeyUp(KeyCode.RightControl)) //If Key is held up/Released
        {
            anim.SetBool("BoxBox", false);
            tD.Stop(); //Timer Stops
            control.movementImpaired = false; //Movement is not impaired
            isHeld = false; //Key is not held anymore
            Shoot(); //Shoot() is called
            acceleration.x = 0; //Acceleration goes back to zero
        }
    }

    public void ChargeDuration(object Source, System.EventArgs e)
    {
        chargeDuration += 1; //Charging Duration is increased every 1 second
    }

    void Shoot()
    {
        Player_controls control = Player.GetComponent<Player_controls>();
        if (chargeDuration >= 100f)
        {
            tempX = Input.GetAxis("Horizontal"); //Gets temporary direction and determines the direction
            if (tempX < 0)
            {
                tempX = -1;
            }
            if (tempX > 0)
            {
                tempX = 1;
            }
            if (tempX == 0)
            {
                tempX = 0;
            }
            acceleration = new Vector2(tempX * 10000, control.rb.velocity.y); //Acceleration force
            control.rb.AddForce(acceleration); //Add acceleration
        }
         //Resets the timer
    }
	// Update is called once per frame
	void Update () {
        Charge();
    }
}
