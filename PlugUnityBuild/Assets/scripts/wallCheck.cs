﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallCheck : MonoBehaviour
{ //checks if collidier is touching a wall

    public bool wallTouch = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "wall") //check for wall
        {
            wallTouch = true;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "wall") //should fix potential issue of colliding with wall and a non wall and another wall
        {
            wallTouch = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "wall") //check if wall leaves
        {
            wallTouch = false;
        }
    }
}
