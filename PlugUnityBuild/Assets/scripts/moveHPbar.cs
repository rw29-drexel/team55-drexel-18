﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveHPbar : MonoBehaviour {
	public Health dr;
	public RectTransform rt;
	public void Update () {
		rt.localScale = new Vector3 (((float)dr.HP / dr.maxHP), rt.localScale.y, rt.localScale.z);
	}
}
