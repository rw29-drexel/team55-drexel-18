﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class contactDamage : MonoBehaviour {
	public int damage;
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Player") {
			other.GetComponent<Health> ().TakeDamage (damage);
		}
	}
	void OnTriggerStay2D(Collider2D other) {
		if (other.gameObject.tag == "Player") {
			other.GetComponent<Health> ().TakeDamage (damage);
		}
	}
}
