﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendIn : MonoBehaviour {
    System.Timers.Timer t, tC;
    public int duration;
    public bool abilityOnCool = false;
    public bool usingAbility = false;
    public int cooldown;
    public bool abilityDone;
    public GameObject player;
    private SpriteRenderer spriteRenderer;


	// Use this for initialization
	void Start () {
        cooldown = 3;
        abilityDone = false;
        duration = 3;
	}
	
    void BecomeEnemy()
    {
        player = GameObject.Find("Player"); //Find the object with tag "Player"
        player.GetComponent<SpriteRenderer>().color = Color.red; //The sprite should be changed here!!!
        player.tag = "Enemy"; //The tag becomes "Enemy" so that enemies can not do damage
    }

    void BecomePlayer()
    {
        player = GameObject.Find("Player");//If enemies get enemy tag, this line should be changed to finding the name of the object itself!!!!
        player.GetComponent<SpriteRenderer>().color = Color.white;   //The sprite should be turned back
        player.tag = "Player"; //The tag becomes "Player"
        Start(); //Initialize all variables
        abilityOnCool = true; //Ability is on cooldown
    }

    void AbilityDuration()
    { 
        t = new System.Timers.Timer();
        t.Interval = 1000; //1 Second
        t.Elapsed += new System.Timers.ElapsedEventHandler(Duration); //subtracts each second from the duration of ability
        t.AutoReset = true; // fires events at every 1 interval
        t.Enabled = true; //Start the timer
    }

    public void Duration(object source, System.EventArgs e) //What happens as each interval(1 sec.) of duration ticks
    {
        if (duration > 0)
        {
            duration = duration - 1; // Duration will be reduced by 1 sec
        }
        if (duration <= 0)
        {
            abilityOnCool = true;   //Ability is on cooldown
            abilityDone = true;     //ability is done
            usingAbility = false;   //Ability is not being used anymore
            t.Stop(); //Stop the timer
            AbilityCooldown();  //The cooldown timer starts to tick

        }
    }
    void AbilityCooldown() //The cooldown timer
    {
        tC = new System.Timers.Timer();
        tC.Interval = 1000; //1 Second
        tC.Elapsed += new System.Timers.ElapsedEventHandler(Cooldown); //subtracts each second from the cooldown of ability
        tC.AutoReset = true; // fires events at every 1 interval
        tC.Enabled = true; //Start the timer
    }

    public void Cooldown(object source, System.EventArgs e) //What happens as each interval(1 sec.) of the cooldown ticks
    {
        if (cooldown > 0)
        {
            cooldown = cooldown - 1;    //cooldown is reduced by 1 second
        }
        if (cooldown <= 0)
        {
            tC.Stop(); //Stop the timer
            abilityOnCool = false; //Ability is not on cooldown anymore
        }
    }

	// Update is called once per frame
	void Update () {
        {
            if (abilityOnCool == false && usingAbility == false)
            {
                if (Input.GetKeyDown(KeyCode.LeftAlt) || (Input.GetKeyDown(KeyCode.RightAlt)))
                    {
                        BecomeEnemy(); //The player becomes an Enemy
                        AbilityDuration();//The duration of ability starts to tick
                        usingAbility = true;
                    }
            }
            if (abilityDone == true)
            {
                BecomePlayer();
            }
        }
    }
}
