﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public int damage; //damage tanken
    public Transform enemyTransform; //transform of the enemy
    public Vector2 vector; //vector between the player and the enemy
    public Transform playerTransform; //player transform
    public Rigidbody2D enemyRb; //enemy rigidbody
    public GameObject enemy; // enemy
    public GameObject player; //player
    public float distance; //distance between player and enemy
    public bool playerIsFound; //checks if player is found
    public Vector2 verticalVector; //vector used to take the enemy up
    public Vector2 distanceVector; //distance between the two objects in vector
    public Transform initial; //initial position of the enemy
    private float x; //x value of initial position
    private float y; //y value of initial position
    public Vector2 returnVector; //vector to go back to the initial position

    void GetInitialPosition() //Getting the enemy's initial location
    {
        initial = enemy.transform;
        x = initial.position.x;
        y = initial.position.y;
    }


    void GetPlayerLocation() //Getting player's location
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerTransform = player.transform;
        playerIsFound = true;
        if (GameObject.FindWithTag("Player") == null) // If player is not found
        {
            playerIsFound = false;
        }
    }

    void GetEnemyLocation() //Getting enemy's current location
    {
        enemyTransform = enemy.transform;
    }

    void GetVector() //Getting the vector representation of the distance between the enemy and the player
    {
        GetPlayerLocation();
        GetEnemyLocation();
        vector = new Vector2(playerTransform.position.x - enemyTransform.position.x, playerTransform.position.y - enemyTransform.position.y);
    }

    void Move() //Moving the air unit
    {
        GetVector(); //Get the distance vector
        enemyRb = enemy.GetComponent<Rigidbody2D>();
        if (vector.y >= -2.7) //If the enemy is below the player, move it up first
        {
            verticalVector = new Vector2(0, 1);
            enemyRb.velocity = verticalVector * 7f;
        }
        if (vector.y < -2.7) //follow the player
        {
            enemyRb.velocity = vector.normalized * 6.5f;
        }
    }
    void ReturnBack() //Return back to its initial position
    {
        if (enemyTransform.position.x == x && enemyTransform.position.y == y) // When the enemy is back to its position
        {
            enemyRb.velocity = returnVector.normalized * 0;
        }
        GetEnemyLocation(); //Else, get its location
        returnVector = new Vector2(x - enemyTransform.position.x, y - enemyTransform.position.y); //get the return vector
        enemyRb.velocity = returnVector.normalized * 5; // move it back
    }

void GetDistance() //Get the distance between the player and the enemy
    {
        distanceVector = playerTransform.position - enemyTransform.position;
        distance = distanceVector.magnitude;
        if (distance > 6) //If distance is greater than 5
        {
            playerIsFound = false;
        }
    }
    // Use this for initialization
    void Start()
    {
        playerIsFound = false;
        GetInitialPosition();

    }
    // Update is called once per frame
    void Update()
    {
        GetDistance();
        if (distance <= 6 && player.tag == "Player")
        {
            playerIsFound = true;
            Move(); //Move the enemy
            Debug.Log(distance);
            if (distance <= 2.89f) //If the distance between the player and the enemy is close
            {
                player.GetComponent<Health>().TakeDamage(damage); //Take damage
            }
        }
        if (playerIsFound == false || player.tag == "Enemy") //If player is not found
        {
            ReturnBack();   //Go back to its original position
        }
    }
}
