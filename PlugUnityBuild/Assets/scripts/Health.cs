﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

	public int HP; //current HP and starting HP
	public int maxHP; //max HP that can't be healed past;
	public int invulFrames; //how long iframes are
	public int timer=0; //counts down for iframes
	public Renderer blink;

	public void kill(){
		if (transform.gameObject.tag == "Player") { // will play game over screen or respawn
			//work on

		}
		Destroy (transform.gameObject); //kill this entity
	}

	// Update is called once per frame
	void Update () {
		if (timer > 0) {
			timer--;
			blink.enabled = !blink.enabled; //alternating blinking to show invul time
			if (timer == 0) { 
				blink.enabled = true;
			}
		}
	}

	void FixedUpdate () {
		if (HP <= 0) {
			kill ();
		}
	}

	public void Heal(int power){
		if (HP + power <= maxHP) { // make sure you can't go past maxHP
			HP += power;
		} else if (HP < maxHP) { // if heal would go past maxHP just set HP to max
			HP = maxHP;
		}
	}

	public void TakeDamage(int dmg){
		if (timer == 0) { //check if iframes are over
			HP -= dmg;
			timer = invulFrames; //give iframes
			if (HP <= 0) { //failsafe just incase game refuses to update for some reason
				kill();
			}
		}
	}
}
