﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class JumpThoughs : MonoBehaviour{
	GameObject platform = new GameObject ();
	public JumpThoughs(int horizontal, int vertical, Sprite sprite1, Transform hold){
		platform.AddComponent<BoxCollider2D> ();
		platform.AddComponent<PlatformEffector2D> ();
		platform.AddComponent<SpriteRenderer> ();
		platform.GetComponent<Transform> ().position = new Vector3 (horizontal, vertical, 0);
		platform.GetComponent<Transform> ().localScale = new Vector3 (1.5f, 0.25f, 0f);
		platform.GetComponent<BoxCollider2D>().usedByEffector=true;
		platform.GetComponent<BoxCollider2D> ().size = new Vector2 (1, 1);
		platform.GetComponent<PlatformEffector2D> ().useOneWay = true;
		platform.GetComponent<PlatformEffector2D> ().surfaceArc = 160;
		platform.GetComponent<PlatformEffector2D> ().useColliderMask = true;
		platform.GetComponent<SpriteRenderer> ().sprite = sprite1;
		platform.GetComponent<SpriteRenderer> ().color = new Color (0, 0, 0, 255);
		platform.tag = "wall";
		platform.transform.parent = hold;
	}
}

public class generatorLayout : MonoBehaviour {
	public Sprite sprite1;
	public Transform hold;
	void Start () {
		int test = 0;
		int counter = 0;
		bool looper = true;
		while (looper) {
			test = Random.Range(0,5);
			counter++;
			if (test < 6) {
				test = Random.Range (0,5);
				new JumpThoughs (counter * 3, test, sprite1, hold);
			}
			if (counter > 5) {
				looper = false;
			}
		}
	}
}
